-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: 172.16.99.3    Database: g.david
-- ------------------------------------------------------
-- Server version	5.5.43-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adherent` (
  `num` smallint(6) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL DEFAULT '',
  `prenom` varchar(25) NOT NULL DEFAULT '',
  `adrRue` varchar(50) DEFAULT NULL,
  `adrCP` mediumint(5) DEFAULT NULL,
  `adrVille` varchar(25) DEFAULT NULL,
  `tel` varchar(10) DEFAULT NULL,
  `mel` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adherent`
--

LOCK TABLES `adherent` WRITE;
/*!40000 ALTER TABLE `adherent` DISABLE KEYS */;
INSERT INTO `adherent` VALUES (1,'Deltour','Charles','4 rue du Pont',72400,'La ferté bernard','02...','cdeltour@hotmail.com'),(2,'Fime','Nadia','5 rue du Montparnasse',72110,' Bonnetable ','06...',NULL),(3,'Ertau','Frank','4 Avenue du président Wilson',72110,'Courcemont','02...','frank.ertau@laposte.net'),(4,'Maneur','David','6 rue Charles Péguy',72110,'Courcival','02...',NULL),(5,'Berezovski','Sylvie','18 rue du Château',61260,'Mâle','02...',NULL),(6,'Finley','Pascale','25 rue de Tolbiac',75013,'Paris','01...','pascfinley@yahoo.fr'),(7,'Vofur','Hector','18 rue Deparcieux',72110,'Bonnetable','02...','hvofur@free.fr'),(8,'Dezou','Fred','3 rue des fleurs',72000,'Le Mans','02...','ozala@aol.com'),(9,'Serty','Julie','23 rue du Calvaire',61130,'Saint-germain-coudre','02...',NULL),(10,'Vofur','Victor','18 rue Deparcieux',72580,'Nogent le bernard','02...','victor.vofur@laposte.net'),(11,'Calende','Hugo','22 rue des jardins',72110,'Courcival','01...',NULL),(12,'Jemba','Hubert','10 rue du 8 mai 1945',72110,'Roupeyrroux le coquet','02...','jaimeba@yahoo.fr'),(13,'Morin','Séverine','4 rue du bac',72400,'La ferté bernard','02...','morinsev@hotmail.com'),(14,'Benrech','Tarek','79 rue de Sèvres',72110,'Courcimont','06...',NULL),(15,'Nguyen','Marc','53 impasse Tourneur',72110,'Roupeyrroux le coquet','02...','nguyen774@wanadoo.fr'),(16,'Louali','Karima','89 rue Poincaré',72110,' Bonnetable','02...','kloua@caramail.fr'),(17,'Paolo','Marco','14 rue du tertre',61130,'Saint-germain-coudre','02...',NULL),(18,'Map','Joanna','120 boulevard Voltaire',72000,'Le Mans','06...',NULL),(19,'Koundé','Fatoumata','4 Place Carrée',72110,'Roupeyrroux le coquet','02...',NULL),(20,'Derissam','Bachir','1 rue des Indes',72110,'Bonnetable','02...',NULL);
/*!40000 ALTER TABLE `adherent` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-14 16:07:36
